<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try {
			$db = new PDO('mysql:dbname=test;host=localhost;charset=utf8mb4', 'root', '');
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db=$db;
			} catch (PDOException $e)
			{
				echo $e->getMessage();
			}
            // Create PDO connection
		}
    }
    
    /** Function returning the complete list of book in the collection. book are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		try{
			
		$stmt = $this->db->query('SELECT * FROM test.book');
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$booklist[]= new Book($row['title'], $row['author'], $row['description'], $row['id']);
			}
		} catch (PDOException $e)
			{
				echo $e->getMessage();
			}
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
		try{
			$stmt =$this->db->prepare("SELECT * FROM test.book WHERE id='$id'");
			$stmt->execute(array($id));
			if($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$book= new Book($row['title'], $row['author'], $row['description'], $row['id']);
				} 
			}
		catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
			if($book->title !="" && $book->author !="") {
			if($book->description== ''){
				$book->description= NULL;
			}
			
			$stmt =$this->db->prepare("INSERT INTO test.book (title, author, description) 
										VALUES (:title, :author, :description)");
			$stmt->execute(array(
				":title"=>$book->title, 
				":author"=>$book->author, 
				":description"=>$book->description));
				$book->id =$this->db->lastInsertId();
			}
			else{
				echo 'Title and author cant be empty';
			}
		
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		try{
			
			if($book->title !='' && $book->author !=''){
			$stmt= $this->db->prepare("UPDATE test.book SET title=?, author=?, description=? WHERE id=?");
			$stmt->execute(array($book->title, $book->author, $book->description, $book->id));
			} else {
				echo 'Title and author cant be empty';
			}
			
		} catch (PDOException $e)
			{
				echo $e->getMessage();
			}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$sql= "DELETE FROM book WHERE id =:id";
		$query=$this->db->prepare($sql);
		echo "id = $id";
		$query->execute( array (":id" => $id));
    }
	
}

?>